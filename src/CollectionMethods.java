package src;

import java.util.*;

public class CollectionMethods 
{ 
	/**
	 * method that returns an arrayList of planets that have a radius larger or equal to the size
	 * @param planets collection of planets
	 * @param size of the planets
	 * @return arrayList that has been sorted
	 */
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size)
	{
		ArrayList<Planet> newPlanets = new ArrayList<Planet>();
		
		for(Planet p : planets)
		{
			if(p.getRadius() >= size)
			{
				newPlanets.add(p);
			}
		}
		return newPlanets;
	}
}
