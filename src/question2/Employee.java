package src.question2;
/**
 * creating an Employee interface
 * @author 1834578
 *
 */
public interface Employee 
{
	double getWeeklyPay();
}
