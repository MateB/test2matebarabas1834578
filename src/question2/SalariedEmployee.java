package src.question2;

public class SalariedEmployee implements Employee 
{
	/**
	 * yearly salary of emp
	 */
	private double yearSalary;
	/**
	 * constructor that initializes Salaried Employee
	 * @param yearSalary yearly salary
	 */
	public SalariedEmployee(double yearSalary)
	{
		this.yearSalary=yearSalary;
	}
	/**
	 * get method to get yearly salary
	 * @return the yearly salary of an employee
	 */
	public double getYearSalary()
	{
		return this.yearSalary;
	}
	/**
	 * method that calculates yearly salary and returns it
	 */
	public double getWeeklyPay()
	{
		return this.yearSalary/52;
	}
}
