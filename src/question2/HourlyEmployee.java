package src.question2;

public class HourlyEmployee implements Employee
{
	/**
	 * hours per week that someone worked
	 */
	private double hrsWeek;
	/**
	 * amount they are paid per hour
	 */
	private double hrsPay;
	/**
	 * initilizes an hourlyEmployee constructor
	 * @param hrsWeek number of hours they worked per week
	 * @param hrsPay salary per hour
	 */
	public HourlyEmployee(double hrsWeek, double hrsPay)
	{
		this.hrsWeek=hrsWeek;
		this.hrsPay=hrsPay;
	}
	/**
	 * get method for salary per hour
	 * @return hourly salary
	 */
	public double getHrsPay()
	{
		return this.hrsPay;
	}
	/**
	 * get method for the hours a week they worked
	 * @return nummber of hours per week
	 */
	public double getHrsWeek()
	{
		return this.hrsWeek;
	}
	/**
	 * method calculates amount they made in a week
	 */
	public double getWeeklyPay()
	{
		return this.hrsPay*this.hrsWeek;
	}
}
