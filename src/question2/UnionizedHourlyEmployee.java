package src.question2;

public class UnionizedHourlyEmployee extends HourlyEmployee
{
	/**
	 * maximum of hours they can work per week
	 */
	private double maxHrsPerWeek;
	/**
	 * the rate at which they get paid after they exceed the maximum hours
	 */
	private double overtimeRate;
	/**
	 * 
	 * @param hrsWeek hours of week they worked
	 * @param hrsPay the pay by hour
	 * @param maxHrsPerWeek the maximum hours of week before overtime
	 * @param overtimeRate the overtime rate, amount paid after max hours reached
	 */
	public UnionizedHourlyEmployee(double hrsWeek, double hrsPay, double maxHrsPerWeek, double overtimeRate)
	{
		super(hrsWeek, hrsPay);
		this.maxHrsPerWeek = maxHrsPerWeek;
		this.overtimeRate = overtimeRate;
	}
	/**
	 * get method for maximum hours per week
	 * @return max hours per week before overtime
	 */
	public double getMaxHrsPerWeek()
	{
		return this.maxHrsPerWeek;
	}
	/**
	 * get method for the over time rate
	 * @return over time rate
	 */
	public double getOvertimeRate()
	{
		return this.overtimeRate;
	}
	/**
	 * method that calculates the weekly pay of the employee
	 */
	public double getWeeklyPay()
	{
		//checks if hrsweek <= maxHrsPerWeek if it is then it returns a regular weekly salary
		if(this.getHrsWeek() <= this.maxHrsPerWeek)
		{
			return this.getHrsPay()*this.getHrsWeek(); 
		}
		//if the if didnt work then it will calculate how much they worked and how much they will be paid with the overtime salary
		double overtimeSalary = (this.getHrsWeek() - this.maxHrsPerWeek) * (this.getHrsPay() * this.overtimeRate);
		return (this.maxHrsPerWeek * this.getHrsPay()) + overtimeSalary;
	}
}
