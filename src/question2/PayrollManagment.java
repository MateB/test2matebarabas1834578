package src.question2;

public class PayrollManagment {
	/**
	 * main method that initializes an employee[]
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Employee[] emp = new Employee[5];
		emp[0] = new HourlyEmployee(15, 10);
		emp[1] = new SalariedEmployee(24500);
		emp[2] = new UnionizedHourlyEmployee(15,10,30,1.5);
		emp[3] = new UnionizedHourlyEmployee(45,10,30,1.5);
		emp[4] = new HourlyEmployee(25, 20);
		
		System.out.println("Total Expenses: " + getTotalExpenses(emp));
		System.out.println(emp[0].getWeeklyPay());
		System.out.println(emp[1].getWeeklyPay());
		System.out.println(emp[2].getWeeklyPay());
		System.out.println(emp[3].getWeeklyPay());
		System.out.println(emp[4].getWeeklyPay());
	}
	/**
	 * method that will calculate the total which employees get paid by adding all the weekly pay' together
	 * @param emp employee[] filled with values 
	 * @return totalexpenses, weekly expenses for all the employees summed up together
	 */
	public static double getTotalExpenses(Employee[] emp)
	{
		double total = 0;
		for (int i = 0; i < emp.length; i++)
		{
			total += emp[i].getWeeklyPay();
		}
		return total;
	}

}
