package src;
import java.util.*;
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	//overriding equals method
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Planet))
		{
			return false;
		}
		else
		{
			Planet p = (Planet)o;
			if(this.name.equals(p.name) && this.planetarySystemName.equals(p.planetarySystemName))
			{
				return true;
			}
		}
		return false;
	}
	//overriding the hashcode method
	@Override
	public int hashCode()
	{
		return this.name.hashCode() + this.planetarySystemName.hashCode();
	}
	//overriding compareTo method so it sorts planets[] by increasing order but if names are the same then it compares by radius and sorts in decreasing order
	@Override
	public int compareTo(Planet p) 
	{
		int compName = this.getName().compareTo(p.getName());
		if (compName == 0)
		{
			return (int) (p.radius - this.radius);
		}
		return compName;
	}
}
